package ru.tsc.felofyanov.tm.api.repository;

import ru.tsc.felofyanov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    List<M> findAll();

    List<M> findAll(Comparator<M> comparator);

    M remove(M model);

    void removeAll(Collection<M> collection);

    void clear();

    M add(M model);

    boolean existsById(String id);

    M findOneById(String id);

    M findOneByIndex(Integer index);

    M removeById(String id);

    M removeByIndex(Integer index);
}


