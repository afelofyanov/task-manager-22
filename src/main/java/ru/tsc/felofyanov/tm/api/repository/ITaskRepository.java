package ru.tsc.felofyanov.tm.api.repository;

import ru.tsc.felofyanov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnerRepository<Task> {
    Task create(String userId, String name);

    List<Task> findAllByProjectId(String userId, String projectId);

    Task create(String userId, String name, String description);
}
