package ru.tsc.felofyanov.tm.service;

import ru.tsc.felofyanov.tm.api.service.IAuthService;
import ru.tsc.felofyanov.tm.api.service.IUserService;
import ru.tsc.felofyanov.tm.enumerated.Role;
import ru.tsc.felofyanov.tm.exception.field.LoginEmptyException;
import ru.tsc.felofyanov.tm.exception.field.PasswordEmptyException;
import ru.tsc.felofyanov.tm.exception.field.UserIdEmptyException;
import ru.tsc.felofyanov.tm.exception.system.AccessDeniedException;
import ru.tsc.felofyanov.tm.exception.system.AuthenticationException;
import ru.tsc.felofyanov.tm.exception.system.LockException;
import ru.tsc.felofyanov.tm.exception.system.PermissionException;
import ru.tsc.felofyanov.tm.model.User;
import ru.tsc.felofyanov.tm.util.HashUtil;

import java.util.Arrays;

public class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(IUserService userService) {
        this.userService = userService;
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new AuthenticationException();
        if (user.isLocked()) throw new LockException();
        if (!user.getPasswordHash().equals(HashUtil.salt(password))) throw new AuthenticationException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public User registry(final String login, final String password, final String email) {
        return userService.create(login, password, email);
    }

    @Override
    public User getUser() {
        if (isAuth()) throw new AccessDeniedException();
        final String userId = getUserId();
        return userService.findOneById(userId);
    }

    @Override
    public String getUserId() {
        if (userId == null) throw new UserIdEmptyException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    public void checkRoles(final Role[] roles) {
        if (roles == null) return;
        final User user = getUser();
        final Role role = user.getRole();
        if (role == null) throw new PermissionException();

        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionException();
    }
}
