package ru.tsc.felofyanov.tm.repository;

import ru.tsc.felofyanov.tm.api.repository.IProjectRepository;
import ru.tsc.felofyanov.tm.model.Project;

public class ProjectRepository extends AbstractUserOwnerRepository<Project> implements IProjectRepository {

    @Override
    public Project create(final String userId, final String name) {
        final Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        return add(project);
    }

    @Override
    public Project create(final String userId, final String name, final String description) {
        final Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        project.setDescription(description);
        return add(project);
    }
}
