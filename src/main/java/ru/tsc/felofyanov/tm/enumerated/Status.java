package ru.tsc.felofyanov.tm.enumerated;

import ru.tsc.felofyanov.tm.exception.field.StatusEmptyException;

public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    private final String displayName;

    Status(String displayName) {
        this.displayName = displayName;
    }

    public static Status toStatus(String value) {
        if (value == null || value.isEmpty()) throw new StatusEmptyException();
        for (final Status status : values())
            if (status.name().equals(value)) return status;
        return null;
    }

    public String getDisplayName() {
        return displayName;
    }
}
